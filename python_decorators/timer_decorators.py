import time
from typing import Callable


def timer_aggfunc(a: float, b: float, aggfunc: Callable) -> float:
    t1 = time.time()  # dekorator
    print(t1)  # dekorator

    add_result = aggfunc(a, b)  # funkcja pierwszego rzędu

    t2 = time.time()  # dekorator
    print(t2)  # dekorator

    print(f'Czas wykonania funkcji {aggfunc.__name__!r}: {(t2 - t1):.2f}s')  # dekorator
    return add_result  # funkcja pierwszego rzędu


def universal_timer(func: Callable) -> Callable:
    def wrapper(*args, **kwargs):
        t1 = time.time()
        result_func = func(*args, **kwargs)
        t2 = time.time()
        print(f'Czas wykonania funkcji {func.__name__!r}: {(t2 - t1):.2f}s')  # dekorator
        return result_func

    return wrapper


@universal_timer
def add(a: float, b: float) -> float:
    time.sleep(1)
    return a + b


@universal_timer
def mult(a: float, b: float) -> float:
    time.sleep(1)
    return a * b


@universal_timer
def only_sleep(secs: float) -> None:
    print("I sleep! Don't wake me up!")
    time.sleep(secs)


if __name__ == "__main__":
    add_result = add(2, 5)
    only_sleep(2)
    mult(2, 5)
