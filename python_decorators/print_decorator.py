from typing import Callable

def print_decorator(func: Callable) -> Callable:
    def wrapper(*args, **kwargs):
        def decorate(input_msg, input_result_func):
            print(input_msg)
            print("=" * len(input_msg))
            print(str(input_result_func).center(len(input_msg), "-"))
            print("=" * len(input_msg))
            print("")

        result_func = func(*args, **kwargs)

        if isinstance(result_func, str):
            msg = f"Result of {func.__name__!r} function:"
        else:
            msg = f"{func.__name__!r} return type {type(result_func)!r} result"

        decorate(msg, result_func)
        return result_func

    return wrapper


@print_decorator
def hello_world():
    return "hello world"


@print_decorator
def add(a, b):
    return a + b


if __name__ == "__main__":
    str_result = hello_world()
    int_result = add(1, 2)

    print(str_result)
    print(type(int_result))
    print(int_result)

