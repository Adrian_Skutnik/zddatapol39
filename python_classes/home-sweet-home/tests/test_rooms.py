import sys
from pathlib import Path

sys.path.append(str(Path(sys.path[0]).parent))

import unittest
from home_sweet_home.spaces.rooms import Room

class TestRoom(unittest.TestCase):

    # GIVEN
    def setUp(self) -> None:
        """ Wykonane przed każdym testem """
        self.room = Room(width=2, length=3, height=2.5)
        print(repr(self.room))

    def tearDown(self) -> None:
        """ Wykonane po każdym teście """
        print(repr(self.room))
        print("")

    def test_room_area(self):
        print("test_room_area")
        self.assertEqual(self.room.area, 6)

    def test_invalid_room_init(self):
        print("test_invalid_room_init")

        with self.assertRaises(ValueError):
            Room(width=-1, length=3, height=2.5)

        with self.assertRaises(ValueError):
            Room(width=1, length="3", height=2.5)

    def test_check_dimension_value(self):
        print("test_check_dimension_value")

        self.room.height = "abc"
        with self.assertRaises(ValueError):
            self.room._check_dimension_value(self.room.height)

        self.room.height = -1
        with self.assertRaises(ValueError):
            self.room._check_dimension_value(self.room.height)

        self.room.height = 5
        result = self.room._check_dimension_value(self.room.height)
        self.assertEqual(result, 5)

if __name__ == "__main__":
    unittest.main()
