from copy import copy
import sys
from pathlib import Path

sys.path.append(str(Path(sys.path[0]).parent.parent))
from home_sweet_home.furniture.wardrobes import Wardrobe


class Room:

    def __init__(self, width: float, length: float, height: float, windows_number: int = 1):
        self.width = self._check_dimension_value(width)
        self.length = self._check_dimension_value(length)
        self.height = self._check_dimension_value(height)

        self.windows_number = windows_number
        self.floor = []
        self.wardrobes = []

    def __repr__(self):
        return f"{self.__class__.__name__}(width={self.width}, depth={self.length}, height={self.height})"


    @staticmethod
    def _check_dimension_value(value):

        if not isinstance(value, (int, float)):
            raise ValueError(f"Wartość wymiaru pokoju musi być liczbowa, a nie typu {type(value)}")

        if value <= 0:
            raise ValueError("Wartość wymiaru musi być dodatnia!")

        return value

    def calculate_area(self):
        return self.width * self.length

    @property
    def area(self):
        pass

    @area.getter
    def area(self):
        return self.calculate_area()

    def add_item(self, item: str):
        if not isinstance(item, str):
            raise ValueError(f"`item` musi być typu string, nie {type(item)}")

        self.floor.append(item)

    def add_wardrobe(self, wardrobe: Wardrobe):

        if not isinstance(wardrobe, Wardrobe):
            raise ValueError(f"`item` musi być typu {Wardrobe}, nie {type(wardrobe)} ")

        self.wardrobes.append(wardrobe)

    def clean(self):

        if not self.floor:
            print("Pokój jest czysty!")
            return None

        iter_wardrobe = iter(self.wardrobes)
        actual_wardrobe: Wardrobe = next(iter_wardrobe)

        items = copy(self.floor)

        for item in items:
            if actual_wardrobe.is_free_space():
                actual_wardrobe.add_item(item)
                self.floor.remove(item)
            else:
                actual_wardrobe = next(iter_wardrobe)
                actual_wardrobe.add_item(item)
                self.floor.remove(item)

























# # import sys
# import logging
# import warnings
# # from pathlib import Path
#
# # sys.path.append(str(Path(sys.path[0]).parent.parent))
# from home_sweet_home.furniture.wardrobes import Wardrobe
#
# class Room:
#
#     total_number_of_rooms = 0
#     rooms_dimensions = []
#
#     def __init__(self, width: float, length: float, height: float, windows_number: int = 1):
#         self.width = self._check_valid_dimension_value(width)
#         self.length = self._check_valid_dimension_value(length)
#         self.height = self._check_valid_dimension_value(height)
#         self.windows_number = windows_number
#         self._check_windows_number()
#
#         self.floor = []
#         self.furniture = []
#
#         self.rooms_dimensions.append({
#             "width": self.width,
#             "length": self.length,
#             "height": self.height,
#         })
#
#     @staticmethod
#     def _check_valid_dimension_value(value):
#         if not isinstance(value, (int, float)):
#             raise ValueError(f"{value} must be `int` or `float`, not {type(value)}")
#
#         if value < 0:
#             raise ValueError(f"{value:.2f} mustn't be negative.")
#
#         if value > 5:
#             warnings.warn(f"Are you shue then your room is bigger then {value:.2f} m?")
#
#         return value
#
#     def _check_windows_number(self):
#         if not self.windows_number:
#             raise ValueError("Your room need a window!")
#
#         if self.windows_number > 5:
#             logging.warning("Are you sure have more then 5 windows in your room?")
#
#
#     # def calculate_area(self):
#     #     self.area = self.width * self.length
#     #     print(f"Area of this room is {self.width:.2f}m * {self.length:.2f}m = {self.area:.2f} m2")
#
#     @property
#     def area(self): ...
#
#     @area.getter
#     def area(self):
#         return self.width * self.length
#
#     def print_area(self):
#         print(f"Area of this room is {self.width:.2f}m * {self.length:.2f}m = {self.area:.2f} m2")
#
#     def add_item(self, item: str):
#         """Append item on the floor in my room."""
#         self.floor.append(item)
#
#     def add_wardrobe(self, wardrobe: Wardrobe):
#         if not isinstance(wardrobe, Wardrobe):
#             raise ValueError(f"{wardrobe} is not a Wardrobe")
#
#         if wardrobe.width > self.width:
#             raise ValueError(f"{wardrobe.__class__.__name__} width is greater then {self.__class__.__name__} width.")
#
#         self.furniture.append(wardrobe)
#
#     @classmethod
#     def calculate_total_area(cls):
#         widths = [dimensions["width"] for dimensions in cls.rooms_dimensions]
#         lengths = [dimensions["length"] for dimensions in cls.rooms_dimensions]
#         total_area = 0
#
#         for width, length in zip(widths, lengths):
#             total_area += width * length
#
#         return total_area
#
# class Bathroom(Room):
#     def __init__(self, width: float, length: float, height: float, windows_number: int = 0):
#         super().__init__(width, length, height, windows_number)
#
#     def _check_windows_number(self):
#         pass
#
#
#
