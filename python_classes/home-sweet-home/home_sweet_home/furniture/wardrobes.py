class Wardrobe:
    def __init__(self, width: float, depth: float, height: float, max_item: int = 5):
        self._width, self._depth, self._height = width, depth, height
        self.max_item = max_item
        self.space = []

    def __repr__(self):
        return f"{self.__class__.__name__}(width={self.width}, depth={self.depth}, height={self.height})"

    def __str__(self):
        return f"{self.__class__.__name__}(W={self.width:.2f}m; D={self.depth:.2f}m; H={self.height:.2f}m)"

    def is_free_space(self):
        return len(self.space) <= self.max_item

    def add_item(self, item: str):
        if not isinstance(item, str):
            raise ValueError(f"`item` musi być typu string, nie {type(item)}")

        self.space.append(item)

    @property
    def width(self):
        ...

    @width.getter
    def width(self):
        return self._width

    @width.setter
    def width(self, value):
        if not isinstance(value, (int, float)):
            raise ValueError(f"Width must be a numeric value, not {type(value)}")
        self._width = value
    @width.deleter
    def width(self):
        print("You can't delete a dimension width")

    @property
    def depth(self):
        ...

    @depth.getter
    def depth(self):
        return self._depth

    @depth.setter
    def depth(self, value):
        if not isinstance(value, (int, float)):
            raise ValueError(f"Depth must be a numeric value, not {type(value)}")
        self._depth = value


    @depth.deleter
    def depth(self):
        print("You can't delete a dimension depth")

    @property
    def height(self): ...

    @height.getter
    def height(self):
        return self._height

    @height.setter
    def height(self, value):
        if not isinstance(value, (int, float)):
            raise ValueError(f"Height must be a numeric value, not {type(value)}")
        self._height = value
    @height.deleter
    def height(self):
        print("You can't delete a dimension height")
