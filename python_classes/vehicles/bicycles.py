MILE_TO_KM = 1.609344
MINUTES_IN_HOUR = 60

class Bicycle:

    def __init__(self, time: float, speed: float, distance: float = 0.0):
        self.time = time
        self.speed = speed

        try:
            self._check_speed()
        except ValueError as value_error:
            self.speed = 0
            raise value_error

        self.distance = distance

    def _check_speed(self):

        if not isinstance(self.speed, (int, float)):
            raise TypeError(f"Prędkość musi być liczbą.")

        if self.speed > 50:
            raise ValueError(f"Maksymalna prędkość to 50 km/h, obecnie {self.speed} km/h")

        if self.speed < 0:
            raise ValueError(f"Prędkość nie może być ujemna, obecnie {self.speed} km/h")

    def add_speed(self, additional_speed: float = 0.0):

        self.speed += additional_speed
        try:
            self._check_speed()
        except ValueError as value_error:
            self.speed -= additional_speed
            raise value_error

    @staticmethod
    def convert_speed(speed_to_convert: float, unit_to: str = "mile/h"):

        if unit_to == "mile/h":
            return speed_to_convert / MILE_TO_KM

        elif unit_to == "km/h":
            return speed_to_convert * MILE_TO_KM

        else:
            raise ValueError(f"Wartość `unit_to` może być równa `mile/h` lub `km/h`, jest {unit_to}")

    def run(self):

        time_in_minute = int(round(self.time * MINUTES_IN_HOUR, 0))
        speed_km_per_minute = self.speed / MINUTES_IN_HOUR

        for minute in range(1, time_in_minute+1):
            self.distance += speed_km_per_minute
            if minute % 10 == 0:
                print(
                    f"Upłynęło {(minute / MINUTES_IN_HOUR):.2f}h "
                    f"Przejechany dystans {self.distance:.2f} km \n"
                )

            self.time -= 1/MINUTES_IN_HOUR

        self.speed = 0














